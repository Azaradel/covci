# Covci

Prototype for stdout wrapper looking for minimum % of coverage by given regex and set up limits for minimum value.

### Planned usage:
```
$ command_running_test_with_coverage_report | covci regex minimum_percentage
```
---
```
$ covci path/to/file regex minimum_percentage
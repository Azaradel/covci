all: build

build:
	go build -o bin/covci

install: build
	cp bin/covci ~/.bin/

test: build
	go test -cover ./...

lint:
	golint -set_exit_status $(go list ./...)

format:
	go fmt

clean:
	rm bin/covci

.PHONY: build test clean
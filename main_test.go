package main

import (
	"regexp"
	"testing"
)

func TestGetDeepestSubmatch(t *testing.T) {
	coverageRaportMock := []byte(`Summary:
  Classes: 26.67% (20/75)
  Methods: 44.44% (120/270)
	Lines:   33.12% (310/936)
`)

	t.Run("return regex match", func(t *testing.T) {
		reg := regexp.MustCompile(`\s+Lines:(\s+(\d+.\d+).*)`)
		got := GetDeepestSubmatch(reg, coverageRaportMock)
		want := "33.12"

		assertString(t, got, want)
	})
	t.Run("return deepest regex match", func(t *testing.T) {
		reg := regexp.MustCompile(`\s+(Lines):((\s+(\d+.\d+)).*)`)
		got := GetDeepestSubmatch(reg, coverageRaportMock)
		want := "33.12"

		assertString(t, got, want)
	})
}

func TestCompare(t *testing.T) {
	regexResultMock := "45.11"

	t.Run("return true if minimum was reached", func(t *testing.T) {
		want := true
		got, _ := Compare(regexResultMock, "30")

		assert(t, got, want)

		got, _ = Compare(regexResultMock, "40.0")

		assert(t, got, want)
	})
	t.Run("return false if minimum was not reached", func(t *testing.T) {
		want := false
		got, _ := Compare(regexResultMock, "50")

		assert(t, got, want)

		got, _ = Compare(regexResultMock, "65.00")

		assert(t, got, want)
	})
}

func assert(t *testing.T, got, want bool) {
	t.Helper()
	if got != want {
		t.Errorf("got %v, want %v", got, want)
	}
}

func assertString(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got '%s', want '%s'", got, want)
	}
}

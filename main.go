package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	info, err := os.Stdin.Stat()
	if err != nil {
		log.Fatal(err)
	}

	if info.Mode()&os.ModeCharDevice != 0 {
		fmt.Println("input through pipe was not found")
		os.Exit(1)
	}

	if len(os.Args[1:]) < 2 {
		fmt.Println("you need to provide regex and minimum % of code coverage as a argument")
		os.Exit(1)
	}
	regexPattern := os.Args[1]
	minimumPercantage := os.Args[2]

	reg, err := regexp.Compile(regexPattern)
	if err != nil {
		log.Fatalf("could not compile regex pattern: %v", err)
	}

	reader := bufio.NewReader(os.Stdin)
	buffer := &bytes.Buffer{}

	_, err = buffer.ReadFrom(reader)
	if err != nil {
		log.Fatalf("could not read from stdin: %v", err)
	}

	match := GetDeepestSubmatch(reg, buffer.Bytes())
	if match == "" {
		log.Fatal("could not find the code coverage")
	}
	success, err := Compare(match, minimumPercantage)
	if err != nil {
		log.Fatal(err)
	}

	if !success {
		fmt.Println("Failure!")
		os.Exit(1)
	}
	fmt.Println("Success!")
	os.Exit(0)
}

func GetDeepestSubmatch(reg *regexp.Regexp, output []byte) string {
	parsedOutput := string(output)
	fmt.Println(parsedOutput)

	result := reg.FindAllStringSubmatch(parsedOutput, 3)
	if result == nil {
		return ""
	}

	deep := len(result[0])
	return result[0][deep-1]
}

func Compare(got, minimum string) (bool, error) {
	bitsize := 32

	codeCoveragePercentage, err := strconv.ParseFloat(got, bitsize)
	if err != nil {
		return false, fmt.Errorf("could not parse regex result: %v", err)
	}

	minimumParsed, err := strconv.ParseFloat(minimum, bitsize)
	if err != nil {
		return false, fmt.Errorf("could not parse minimum value: %v", err)
	}

	return codeCoveragePercentage >= minimumParsed, nil
}
